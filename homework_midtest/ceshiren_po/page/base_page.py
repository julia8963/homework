import time

from selenium import webdriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class basePage:
    __BASE_URL = 'https://ceshiren.com/'

    def __init__(self, base_driver=None):
        if base_driver:
            self.driver = base_driver
        else:
            self.driver = webdriver.Chrome()
            self.driver.implicitly_wait(5)
            self.driver.maximize_window()

        if not self.driver.current_url.startswith('http'):
            self.driver.get(self.__BASE_URL)
            time.sleep(20) #临时登陆用

    def do_quit(self):
        self.driver.quit()

    def do_find(self, by, locator=None):
        if locator:
            return self.driver.find_element(by, locator)
        else:
            return self.driver.find_element(*by)

    def do_finds(self, by, locator=None):
        if locator:
            return self.driver.find_elements(by, locator)
        else:
            return self.driver.find_elements(*by)

    def do_send_keys(self, value, by, locator=None):
        ele = self.do_find(by, locator)
        ele.clear()
        ele.send_keys(value)

    def wait_visible(self, locator: tuple):
        WebDriverWait(self.driver, 10, 2). \
            until(expected_conditions.visibility_of_element_located(locator))

    def wait_clickable(self, locator: tuple):
        WebDriverWait(self.driver, 10, 2). \
            until(expected_conditions.element_to_be_clickable(locator))

    def wait_exist(self, locator):
        WebDriverWait(self.driver, 10, 2). \
            until(expected_conditions.presence_of_element_located(locator))
