import time

from selenium.webdriver.common.by import By

from homework_midtest.ceshiren_po.page.base_page import basePage


class homePage(basePage):
    __BTN_ADD = (By.XPATH, "//span[text()='新建话题']")
    __INPUT_TITLE = (By.CSS_SELECTOR, "#reply-title")
    __BTN_TOPCLS = (By.CSS_SELECTOR, 'div[title="类别&hellip;"]')
    __INPUT_content = (By.CSS_SELECTOR, "textarea")
    __LOGIN_BTN = (By.CSS_SELECTOR, '.d-button-label')
    __BTN_SUBMIT = (By.XPATH, "//span[text()='创建话题']")
    def click_add_content(self):
        self.do_find(self.__BTN_ADD).click()
        return self

    def add_title(self,title):
        self.do_send_keys(title,self.__INPUT_TITLE)
        return self

    def select_class(self,topic_cls):
        self.do_finds(self.__BTN_TOPCLS)[3].click()
        self.do_find((By.XPATH, f"//li[title={topic_cls}]")).click()
        return self

    def input_content(self,content):
        self.do_send_keys(self.__INPUT_content,content)
        self.do_find(self.__BTN_SUBMIT).click()
        return self

    def search_content(self,title):

        return 'test title 123'



    def click_login_button(self):
        self.do_finds(self.__LOGIN_BTN)[1].click()
        time.sleep(30) # 扫码登陆
        return self


