'''
这个case还没跑通，不用下载去跑了

'''


from homework_midtest.ceshiren_po.page.home_page import homePage


class TestCreateContent:
    def setup_class(self):
        self.browser = homePage()

    def teardown_class(self):
        self.browser.do_quit()

    def test_add_member(self):
        title='test title 123'
        value = self.browser\
            .click_add_content()\
            .add_title(title)\
            .select_class('学习笔记')\
            .input_content('test content 123')\
            .search_content(title)
        assert title in value