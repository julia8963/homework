"""
@file:homework_midtest.py
@author:'Julia'
@time:7/16/2023
@desc:
"""
from typing import List


def recursive_binary_search(data_list, target):
    if len(data_list) == 0:
        return -1
    else:
        mid = len(data_list) // 2
        if data_list[mid] == target:
            return mid
        elif data_list[mid] > target:
            return recursive_binary_search(data_list[:mid], target)
        else:
            result = recursive_binary_search(data_list[mid+1:], target)
            if result == -1:
                return -1
            else:
                return mid + 1 + result

arr=[2,4,7,9,22,55,77,89]
print(recursive_binary_search(arr, 77))

