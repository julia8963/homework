import yaml
from faker import Faker
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from homework1_web.page.login_page import loginPage


class TestAddMemberFromHome:
    def setup_class(self):
        self.browser = loginPage()
        # self.browser.save_cookies()

    def setup(self):
        self.browser = self.browser.login()
        fake = Faker('zh_CN')
        self.depart = fake.job()+'部'

    def teardown_class(self):
        self.browser.do_quit()

    def test_add_member(self):
        value = self.browser\
            .to_contact_page()\
            .click_add_department()\
            .fill_in_depart_info(self.depart)\
            .get_tips()
        assert '新建部门成功' in value