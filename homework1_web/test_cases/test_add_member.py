import yaml
from faker import Faker
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from homework1_web.page.login_page import loginPage


class TestAddMemberFromHome:
    def setup_class(self):
        self.browser = loginPage()
        self.browser.save_cookies()

    def setup(self):
        self.browser = self.browser.login()
        fake = Faker('zh_CN')
        self.username = fake.name()
        self.accid = fake.ssn()
        self.mobile = fake.phone_number()

    def teardown_class(self):
        self.browser.do_quit()

    def test_add_member(self):
        value = self.browser\
            .click_add_member() \
            .fill_in_info(self.username, self.accid, self.mobile) \
            .get_tips()
        assert '保存成功' in value

    def test_add_member_contact(self):
        value = self.browser\
            .to_contact_page() \
            .click_add_member_in_contact() \
            .fill_in_depart_info(self.username, self.accid, self.mobile) \
            .get_tips()
        assert '保存成功' in value


