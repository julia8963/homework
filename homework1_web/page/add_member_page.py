from selenium.webdriver.common.by import By

from homework1_web.page.base_page import basePage
import logging


class addMemberPage(basePage):

    __input_username = (By.ID, "username")
    __input_accid = (By.ID, "memberAdd_acctid")
    __input_mobile =(By.ID, "memberAdd_phone")
    __btn_save=(By.CSS_SELECTOR,'a.qui_btn.ww_btn.js_btn_save')

    def fill_in_depart_info(self,username, accid, mobile):
        # 3.1输入用户名
        self.do_send_keys(username, self.__input_username)
        # 3.2输入acctid
        self.do_send_keys(accid, self.__input_accid)
        # 3.3输入手机号
        self.do_send_keys(mobile, self.__input_mobile)
        logging.info('点击保存')
        self.do_finds(self.__btn_save)[0].click()

        from homework1_web.page.contact_page import contactPage
        return contactPage(self.driver)
