import time

from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By

from homework1_web.page.base_page import basePage


class contactPage(basePage):
    __loc_tips = (By.ID, "js_tips")
    __BTN_ADD = (By.LINK_TEXT, "添加成员")
    __PLUS_BTN = (By.XPATH, "//i[@class='member_colLeft_top_addBtn']")
    # 添加部门
    __ADD_DEPART_BTN=(By.XPATH, "//a[text()='添加部门']")

    def get_tips(self):
        self.wait_visible(self.__loc_tips)
        tips_value = self.driver.find_element(*self.__loc_tips).text
        return tips_value

    def click_add_member_in_contact(self):
        # time.sleep(2)
        self.wait_exist(self.__BTN_ADD)
        self.wait_visible(self.__BTN_ADD)
        self.wait_clickable(self.__BTN_ADD)
        self.do_find(self.__BTN_ADD).click()

        from homework1_web.page.add_member_page import addMemberPage
        return addMemberPage(self.driver)

    def click_add_department(self):
        self.do_find(self.__PLUS_BTN).click()
        self.do_find(self.__ADD_DEPART_BTN).click()

        from homework1_web.page.add_depart_page import addDepartPage
        return addDepartPage(self.driver)