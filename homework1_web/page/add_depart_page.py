from selenium.webdriver.common.by import By

from homework1_web.page.base_page import basePage
import logging


class addDepartPage(basePage):
    # 部门名称
    __DEPART_BTN = (By.XPATH, "//input[@name='name']")
    # 选择所属部门
    __DEPART_UP_BTN = (By.XPATH, "//span[@class='js_parent_party_name']")
    __DEPART_UP = (By.XPATH, "//div[@class='inputDlg_item']//a[text()='花生plus西柚']")
    # 确定按钮
    __CONF_BTN=(By.XPATH, "//a[text()='确定']")

    def fill_in_depart_info(self, depart_name):
        self.do_send_keys(depart_name, self.__DEPART_BTN)
        self.do_find(self.__DEPART_UP_BTN).click()
        self.do_find(self.__DEPART_UP).click()
        self.do_find(self.__CONF_BTN).click()

        from homework1_web.page.contact_page import contactPage
        return contactPage(self.driver)
