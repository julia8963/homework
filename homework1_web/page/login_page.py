import time

import yaml

from homework1_web.page.base_page import basePage


class loginPage(basePage):
    __BASE_URL = 'https://work.weixin.qq.com/wework_admin/frame'

    def save_cookies(self):
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        time.sleep(10)
        cookies = self.driver.get_cookies()
        with open('../data/cookies.yaml','w') as f:
            yaml.safe_dump(data=cookies,stream=f)



    def login(self):
        # 扫码登录
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # 2、获取本地的cookie
        with open("../data/cookies.yaml", "r") as f:
            cookies = yaml.safe_load(f)
        # 3、植入cookie
        for ck in cookies:
            self.driver.add_cookie(ck)
        # 4、访问企业微信首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")


        from homework1_web.page.home_page import homePage
        return homePage(self.driver)