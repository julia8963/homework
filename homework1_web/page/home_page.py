from selenium.webdriver.common.by import By

from homework1_web.page.base_page import basePage


class homePage(basePage):
    __BTN_ADD = (By.LINK_TEXT, "添加成员")
    __MENU_CONTACTS = (By.ID, 'menu_contacts')

    def click_add_member(self):
        self.do_find(self.__BTN_ADD).click()

        from homework1_web.page.add_member_page import addMemberPage
        return addMemberPage(self.driver)

    def to_contact_page(self):
        self.do_find(self.__MENU_CONTACTS).click()

        from homework1_web.page.contact_page import contactPage
        return contactPage(self.driver)
