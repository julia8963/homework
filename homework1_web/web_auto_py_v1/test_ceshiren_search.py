'''
@file:test_ceshiren_search.py
@author:'Julia'
@time:5/17/2023
@desc:   被测产品地址：https://ceshiren.com/
要求实现搜索功能的Web自动化测试。
Selenium 常用操作与用例编写。
使用隐式等待优化代码。
考虑特殊场景的验证。
    输入内容过长。
    特殊字符。
    其他。
使用参数化优化代码。
'''
import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By


class TestCeshirenSearch:
    def setup_class(self):
        """初始化浏览器"""
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(15)

    def setup(self):
        # self.driver.get('https://ceshiren.com/')
        # self.driver.find_element(By.CSS_SELECTOR,'.d-icon-search').click()
        # self.driver.find_element(By.CSS_SELECTOR, '.d-icon-sliders-h').click()
        self.driver.get('https://ceshiren.com/search?expanded=true')

    def teardown_class(self):
        self.driver.quit()

    @pytest.mark.parametrize('keyword',
                             ['面经', 'appium', '测', 'a',
                              "Appium执行报这个错误：Did not get confirmation UiAutomator2 deleteSession worked; Error was: UnknownError: An unknown server-side error occurred while processing the command. Original error: ‘DELETE /’ cannot be proxied to UiAutomator2 server because the instrume"],
                             ids=['搜索汉字', '搜索英文', '单个中文', '单个英文', '超长字符含中文英文标点符号'
                                  ])
    def test_search_normal(self, keyword):
        self.driver \
            .find_element(By.CSS_SELECTOR, '.full-page-search') \
            .send_keys(keyword)
        self.driver \
            .find_element(By.CSS_SELECTOR, '.search-cta') \
            .click()
        result = self.driver.find_element(By.CSS_SELECTOR, '.topic-title')
        assert keyword.lower() in result.text.lower()

    def test_search_abnormal_1(self):
        # 不输入字符直接搜索
        self.driver \
            .find_element(By.CSS_SELECTOR, '.search-cta') \
            .click()
        result = self.driver.find_element(By.CSS_SELECTOR, '.fps-invalid').text
        assert 'Your search term is too short.' in result

    @pytest.mark.parametrize('keyword',
                             [',', '.', '-', '+', '%', '_'],
                             ids=['搜索,', '搜索.', '搜索-', '搜索+', '搜索%', '搜索_'
                                  ])
    def test_search_abnormal_2(self, keyword):
        # 输入标点符号
        self.driver \
            .find_element(By.CSS_SELECTOR, '.full-page-search') \
            .send_keys(keyword)
        self.driver \
            .find_element(By.CSS_SELECTOR, '.search-cta') \
            .click()
        result = self.driver.find_element(By.CSS_SELECTOR, '.search-results').text
        assert 'No results found.' in result
