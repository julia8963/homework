"""
@file:L1_homework.py
@author:'Julia'
@time:7/16/2023
@desc:
作业内容
编写学员实体类 Student，对应成员变量包含：学号 id、姓名 name、性别 sex；
编写学员管理类 StudentManagement ，实现添加学员方法 addStudent()。
编写StudentManagement的main方法进行学员信息的添加：
学号：1001,姓名：张三,性别：男。
学号：1002,姓名：莉丝,性别：女。
学号：1003,姓名：王武,性别：男。
命令行输出打印效果如下：
什么时候打印？？
"""
import sys
from dataclasses import dataclass
from typing import List


@dataclass
class Student:
    id: int
    name: str
    sex: str


class StudentManagement:

    def __init__(self, students: List[Student]):
        if students:
            self.__students = students
        else:
            self.__students = []

    def getSdudents(self):
        id = int(input("请输入想要查找学员的编号："))
        result = self.get_students_by_id(id)
        if result:
            s = f'''学员信息获取成功，该学员的信息为：
学号：{result.id}，姓名：{result.name}，性别：{result.sex}'''
            print(s)
        else:
            print('查无此人！')

    def get_students_by_id(self, id):
        result = None
        for i in self.__students:
            if i.id == id:
                result = i
        return result

    def addStudent(self):
        id_input = input("请输入学员编号: ")
        name = input("请输入学员姓名: ")
        sex = input("请输入学员性别: ")
        while sex != '男' and sex != '女':
            print('请输入正确的学员性别')
            sex = input("请输入学员性别: ")
        student = Student(id_input, name, sex)
        print(f'添加成功，添加的学员信息为：学号：{id_input}，姓名：{name}，性别：{sex}')
        self.__students.append(student)

    def deleteStudent(self):
        id = int(input("请输入想要删除学员的编号："))
        result = self.get_students_by_id(id)
        self.__students.remove(result)
        print('删除后的学员信息为：')
        self.getAllStudents()

    def getAllStudents(self):
        for i in self.__students:
            print(f'学号：{i.id}，姓名：{i.name}，性别：{i.sex}')

    def main(self):
        s = '''--------------欢迎来到学员信息管理系统--------------
        1. 根据学号查看学员信息
        2. 添加学员
        3. 根据学号删除学员后，查看所有学员信息
        4. 查询当前所有学员的信息
        5. 退出系统
        '''

        print(s)
        while True:
            try:
                choice = int(input('请输入你的选择：'))
            except ValueError as e:
                print("请输入正确的号码！")
                continue
            if choice == 1:
                self.getSdudents()
            elif choice == 2:
                self.addStudent()
            elif choice == 3:
                self.deleteStudent()
            elif choice == 4:
                self.getAllStudents()
            elif choice == 5:
                print('成功退出系统，欢迎下次使用')
                sys.exit()
            else:
                print("请输入正确的号码！")


if __name__ == '__main__':
    s1 = Student(1001, '张三', '男')
    s2 = Student(1002, '莉丝', '女')
    s3 = Student(1003, '王武', '男')
    sm = StudentManagement([s1, s2, s3])
    sm.main()
    # sm.addStudent(1004,'林俊杰','男')
