'''
@file:touch_action.py
@author:'Julia'
@time:5/25/2023
@desc:
'''
from appium.webdriver.common.appiumby import AppiumBy
from selenium.webdriver import ActionChains

from appium import webdriver
from selenium.webdriver.common.actions import interaction
from selenium.webdriver.common.actions.action_builder import ActionBuilder
from selenium.webdriver.common.actions.pointer_input import PointerInput

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '6.0'
desired_caps['deviceName'] = 'KVEIVWTS69KV5T8D'
# com.android.settings/com.android.settings.Settings
desired_caps['appPackage'] = 'cn.kmob.screenfingermovelock'
desired_caps['appActivity'] = 'com.samsung.ui.FlashActivity'

driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
driver.find_element(AppiumBy.ID,'com.miui.home:id/drop_target_bar').click()
actions = ActionChains(driver)
actions.w3c_actions = ActionBuilder(driver, mouse=PointerInput(interaction.POINTER_TOUCH, "touch"))
actions.w3c_actions.pointer_action.move_to_location(120, 280)
actions.w3c_actions.pointer_action.pointer_down()
actions.w3c_actions.pointer_action.pause(1)
actions.w3c_actions.pointer_action.move_to_location(360, 280)
actions.w3c_actions.pointer_action.move_to_location(600, 280)
actions.w3c_actions.pointer_action.move_to_location(600, 520)
actions.w3c_actions.pointer_action.move_to_location(600, 760)
actions.w3c_actions.pointer_action.release()
actions.perform()
