'''
@file:appium_demo.py
@author:'Julia'
@time:5/18/2023
@desc:
'''

from appium import webdriver

desired_caps={}
desired_caps['platformName']='Android'
desired_caps['platformVersion']='6.0'
desired_caps['deviceName']='KVEIVWTS69KV5T8D'
#com.android.settings/com.android.settings.Settings
desired_caps['appPackage']='com.android.settings'
desired_caps['appActivity']='com.android.settings.MainSettings'

driver=webdriver.Remote('http://localhost:4723/wd/hub',desired_caps)
print("启动【设置】应用")
driver.quit()