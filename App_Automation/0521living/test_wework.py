'''
@file:test_wework.py
@author:'Julia'
@time:5/21/2023
@desc:
'''
from time import sleep

from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from faker import Faker
from selenium.common import NoSuchElementException

"""
前提条件：
1、提前注册企业微信管理员帐号
2、手机端安装企业微信
3、企业微信 app 处于登录状态
"""


class TestContact:
    implicitly_wait_time = 5

    def setup_class(self):
        self.faker = Faker("zh_CN")
        # 资源初始化
        # 打开【企业微信】应用
        caps = {}
        caps["platformName"] = "Android"
        caps['platformVersion'] ='6.0'
        caps["deviceName"] = "5864acf27d04"
        caps["appPackage"] = "com.tencent.wework"
        caps["appActivity"] = ".launch.LaunchSplashActivity"
        caps["noReset"] = "true"  #不清缓存
        caps["skipServerInstallation"] = "true" #不重新安装UIautomator2
        caps["dontStopAppOnReset"] = "true" #启动APP时不停止APP
        # 创建driver ,与appium server建立连接，返回一个 session
        # driver 变成self.driver 由局部变量变成实例变量，就可以在其它的方法中引用这个实例变量了
        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", caps)
        self.driver.implicitly_wait(self.implicitly_wait_time)

    def teardown_class(self):
        self.driver.quit()

    def test_addcontact(self):
        # 通讯录添加成员用例步骤:
        #     进入【通讯录】页面
        self.driver.find_element(AppiumBy.XPATH,'//*[@text="通讯录"]').click()
        #     点击【添加成员】
        self.swipe_find('添加成员').click()
        self.driver.implicitly_wait(10)
        #     点击【手动输入添加】
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='手动输入添加']").click()
        #     输入【姓名】【手机号】并点击【保存】
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='姓名']/../*[@text='必填']").send_keys(self.faker.name())
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='手机']/..//*[@text='必填']").send_keys(self.faker.phone_number())
        self.driver.find_element(AppiumBy.XPATH, '//*[@text="保存"]').click()
        # 验证点：
        #     登录成功提示信息
        sleep(2)
        toast_tips = self.driver.find_element(AppiumBy.XPATH, "//*[@class='android.widget.Toast']").text
        assert toast_tips == "添加成功"



    def swipe_find(self,text,max_num=5):
        self.driver.implicitly_wait(1)
        # 滑动查找
        for num in range(max_num):
            try:
                element = self.driver.find_element(AppiumBy.XPATH, f"//*[@text='{text}']")
                sleep(1)
                return element
            except NoSuchElementException as e:
                print("未找到元素")
                # 获取屏幕大小
                size = self.driver.get_window_size()
                width = size.get("width")
                height = size.get("height")

                start_x = width / 2
                start_y = height * 0.8

                end_x = width / 2
                end_y = height * 0.2

                self.driver.swipe(start_x, start_y, end_x, end_y, 2000)
            if num == max_num - 1:
                self.driver.implicitly_wait(10)
                # 执行最大次数，仍然没有找到文本，抛异常
                raise NoSuchElementException(f"找了{num}次，还是没有找到，请确认当前页面是否存在")


