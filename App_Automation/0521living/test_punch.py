'''
@file:test_punch.py
@author:'Julia'
@time:5/21/2023
@desc:
'''
from time import sleep

from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from selenium.common import NoSuchElementException


class TestContact:
    implicitly_wait_time = 5

    def setup_class(self):
        # 资源初始化
        # 打开【企业微信】应用
        caps = {}
        caps["platformName"] = "Android"
        caps['platformVersion'] ='6.0'
        caps["deviceName"] = "5864acf27d04"
        caps["appPackage"] = "com.tencent.wework"
        caps["appActivity"] = ".launch.LaunchSplashActivity"
        caps["noReset"] = "true"  #不清缓存
        caps["skipServerInstallation"] = "true" #不重新安装UIautomator2
        caps["dontStopAppOnReset"] = "true" #启动APP时不停止APP
        caps["autoGrantPermissions"] = "true" #自动赋予系统权限
        # 创建driver ,与appium server建立连接，返回一个 session
        # driver 变成self.driver 由局部变量变成实例变量，就可以在其它的方法中引用这个实例变量了
        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", caps)
        self.driver.implicitly_wait(self.implicitly_wait_time)

    def teardown_class(self):
        self.driver.quit()

    def test_punch(self):
        # 打开【企业微信】应用
        # 进入【工作台】页面
        self.driver.find_element(AppiumBy.XPATH,'//*[@text="工作台"]').click()
        # 点击【打卡】
        self.swipe_find('打卡').click()
        self.driver.implicitly_wait(self.implicitly_wait_time)
        # 选择【外出打卡】tab
        self.driver.find_element(AppiumBy.XPATH, '//*[@text="外出打卡"]').click()
        # 点击【第 N 次打卡】
        self.driver.find_element(AppiumBy.XPATH, '//*[contains(@text,"次外出")]').click()
        # 验证点：提示【外出打卡成功】
        self.driver.find_element(AppiumBy.XPATH, '//*[contains(@text,"外出打卡成功")]')

    def swipe_find(self,text,max_num=5):
        self.driver.implicitly_wait(1)
        # 滑动查找
        for num in range(max_num):
            try:
                element = self.driver.find_element(AppiumBy.XPATH, f"//*[@text='{text}']")
                sleep(1)
                return element
            except NoSuchElementException as e:
                print("未找到元素")
                # 获取屏幕大小
                size = self.driver.get_window_size()
                width = size.get("width")
                height = size.get("height")

                start_x = width / 2
                start_y = height * 0.8

                end_x = width / 2
                end_y = height * 0.2

                self.driver.swipe(start_x, start_y, end_x, end_y, 2000)
            if num == max_num - 1:
                self.driver.implicitly_wait(10)
                # 执行最大次数，仍然没有找到文本，抛异常
                raise NoSuchElementException(f"找了{num}次，还是没有找到，请确认当前页面是否存在")