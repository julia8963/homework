'''
@file:edit_contact_page.py
@author:'Julia'
@time:5/28/2023
@desc:
'''
from appium.webdriver.common.appiumby import AppiumBy

from App_Automation.living_528.Page.base_page import BasePage


class EditContactPage(BasePage):
    INPUT_NAME = AppiumBy.XPATH, "//*[@text='姓名']/../*[@text='必填']"
    INPUT_PHONE_NUMBER = AppiumBy.XPATH, "//*[@text='手机']/..//*[@text='必填']"
    BTN_SAVE = AppiumBy.XPATH, '//*[@text="保存"]'
    BTN_DELETE = AppiumBy.XPATH, '//android.widget.TextView[@text="删除"]'
    def edit_member(self, name, phone_number):
        """
        输入姓名手机号点击保存
        :param name:
        :param phone_number:
        :return:
        """
        self.find_and_send(*self.INPUT_NAME, name)
        self.find_and_send(*self.INPUT_PHONE_NUMBER, phone_number)
        self.find_and_click(*self.BTN_SAVE)

        from App_Automation.living_528.Page.add_member_page import AddMemberPage
        return AddMemberPage(self.driver)

    def delete_member(self):
        self.swipe_find('删除成员').click()
        self.find_and_click(*self.BTN_DELETE)

        from App_Automation.living_528.Page.contact_page import ContactPage
        return ContactPage(self.driver)