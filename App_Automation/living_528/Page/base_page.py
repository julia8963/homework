'''
@file:base_page.py
@author:'Julia'
@time:5/28/2023
@desc:
'''
from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.webdriver import WebDriver
from time import sleep

from selenium.common import NoSuchElementException


class BasePage:

    TOAST_TIPS = AppiumBy.XPATH, "//*[@class='android.widget.Toast']"

    def __init__(self, driver: WebDriver = None):
        """
        初始化driver
        :param driver:
        """
        self.driver = driver

    def find(self, by, value):
        """
        查找元素并返回
        :param by:
        :param value:
        :return:
        """
        return self.driver.find_element(by, value)

    def find_and_click(self, by, value):
        """
        查找元素并点击
        :param by:
        :param value:
        :return:
        """
        self.find(by, value).click()

    def find_and_send(self, by, value, text):
        """
        查找元素并输入
        :param by:
        :param value:
        :param text:
        :return:
        """
        self.find(by, value).send_keys(text)

    def set_implicitly_wait(self, time=1):
        """
        设置隐士等待
        :param time:
        :return:
        """
        self.driver.implicitly_wait(time)

    def get_toast_tips(self):
        """
        获取toast信息
        :return:
        """
        return self.find(*self.TOAST_TIPS).text

    def swipe_find(self, text, max_num=5):
        """
        滑动查找文本text
        :param text:
        :param max_num:
        :return:
        """
        self.set_implicitly_wait(3)
        # 滑动查找
        for num in range(max_num):
            try:
                element = self.driver.find_element(AppiumBy.XPATH, f"//*[@text='{text}']")
                sleep(1)
                return element
            except NoSuchElementException as e:
                print("未找到元素")
                # 获取屏幕大小
                size = self.driver.get_window_size()
                width = size.get("width")
                height = size.get("height")

                start_x = width / 2
                start_y = height * 0.8

                end_x = width / 2
                end_y = height * 0.2

                self.driver.swipe(start_x, start_y, end_x, end_y, 2000)
            if num == max_num - 1:
                self.set_implicitly_wait(10)
                # 执行最大次数，仍然没有找到文本，抛异常
                raise NoSuchElementException(f"找了{num}次，还是没有找到，请确认当前页面是否存在")