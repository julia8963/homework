'''
@file:contact_page.py
@author:'Julia'
@time:5/28/2023
@desc:
'''
from time import sleep

from appium.webdriver.common.appiumby import AppiumBy

from App_Automation.living_528.Page.base_page import BasePage

class ContactPage(BasePage):
    TEXT_SEARCH = AppiumBy.XPATH, "//android.widget.EditText"
    ICON_SEARCH = AppiumBy.XPATH, '//android.widget.TextView[@text="花生plus西柚"]/../../../following-sibling::*[1]/child::*[1]'
    TEXT_SEARCH_RESULT = AppiumBy.XPATH, "//android.widget.TextView[@text='联系人']/../following-sibling::*[1]"
    TEXT_NO_RESULT = AppiumBy.XPATH, "//android.widget.TextView[@text='无搜索结果']"
    def go_to_add_member_page(self):
        self.swipe_find('添加成员').click()
        self.set_implicitly_wait(10)
        from App_Automation.living_528.Page.add_member_page import AddMemberPage
        return AddMemberPage(self.driver)

    def search_contact(self,name):
        self.find_and_click(*self.ICON_SEARCH)
        self.find_and_send(*self.TEXT_SEARCH, name)
        self.find_and_click(*self.TEXT_SEARCH_RESULT)
        from App_Automation.living_528.Page.member_info_page import MemberInfoPage
        return MemberInfoPage(self.driver)

    def get_no_result(self,name):
        sleep(30)
        self.find_and_send(*self.TEXT_SEARCH, name)
        return self.find(*self.TEXT_NO_RESULT).text