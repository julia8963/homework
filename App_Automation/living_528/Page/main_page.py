'''
@file:main_page.py
@author:'Julia'
@time:5/28/2023
@desc:
'''
from appium.webdriver.common.appiumby import AppiumBy

from App_Automation.living_528.Page.base_page import BasePage


class MainPage(BasePage):
    TAB_CONTACT = AppiumBy.XPATH, '//*[@text="通讯录"]'

    def go_to_contact_page(self):
        self.find_and_click(*self.TAB_CONTACT)

        from App_Automation.living_528.Page.contact_page import ContactPage
        return ContactPage(self.driver)
