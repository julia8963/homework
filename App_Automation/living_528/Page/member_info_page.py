'''
@file:member_info_page.py
@author:'Julia'
@time:5/28/2023
@desc:
'''
from appium.webdriver.common.appiumby import AppiumBy

from App_Automation.living_528.Page.base_page import BasePage


class MemberInfoPage(BasePage):
    ICON_MEMBER_INFO = AppiumBy.XPATH, "//android.widget.TextView[@text='个人信息']/../../../../following-sibling::*"
    TEXT_EDIT_MEMBER = AppiumBy.XPATH, "//android.widget.TextView[@text='编辑成员']"
    def go_to_edit_member_page(self):
        self.find_and_click(*self.ICON_MEMBER_INFO)
        self.find_and_click(*self.TEXT_EDIT_MEMBER)
        from App_Automation.living_528.Page.edit_contact_page import EditContactPage
        return EditContactPage(self.driver)
