'''
@file:add_member_page.py
@author:'Julia'
@time:5/28/2023
@desc:
'''
from appium.webdriver.common.appiumby import AppiumBy

from App_Automation.living_528.Page.base_page import BasePage


class AddMemberPage(BasePage):
    TEXT_ADDMEMBER = AppiumBy.XPATH, "//*[@text='手动输入添加']"

    def go_to_edit_member_page(self):
        self.find_and_click(*self.TEXT_ADDMEMBER)

        from App_Automation.living_528.Page.edit_contact_page import EditContactPage
        return EditContactPage(self.driver)
