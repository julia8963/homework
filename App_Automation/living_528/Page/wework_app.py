'''
@file:wework_app.py
@author:'Julia'
@time:5/28/2023
@desc:
'''
import time

from appium import webdriver

from App_Automation.living_528.Page.base_page import BasePage


class App(BasePage):

    def start(self):
        """
        启动app
        :return:
        """
        if self.driver == None:
            # 准备资源
            # 定义一个字典
            caps = {}
            # 被测手机的平台
            caps["platformName"] = "Android"
            caps["deviceName"] = "127.0.0.1:11509"
            # 包名和activity名称
            # adb logcat ActivityManager:I | findstr "cmp"
            # adb logcat ActivityManager:I | grep "cmp"
            caps["appPackage"] = "com.tencent.wework"
            caps["appActivity"] = ".launch.LaunchSplashActivity"
            # 防止清空缓存
            caps["noReset"] = "true"
            # caps["skipServerInstallation"] = "true"  # 不重新安装UIautomator2
            # 初始化driver
            self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
            # 设置隐式等待
            self.set_implicitly_wait(10)
            time.sleep(15) #
        else:
            # 只启动当前测试的应用
            self.driver.launch_app()
        return self

    def restart(self):
        """
        重启
        :return:
        """
        pass

    def stop(self):
        """
        停止app
        :return:
        """
        self.driver.quit()

    def goto_main(self):
        """
        进入app首页
        :return:
        """

        from App_Automation.living_528.Page.main_page import MainPage
        return MainPage(self.driver)
