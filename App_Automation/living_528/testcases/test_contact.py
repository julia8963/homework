'''
@file:test_contact.py
@author:'Julia'
@time:5/28/2023
@desc:
'''
import faker
from App_Automation.living_528.Page.wework_app import App

class TestContact():

    def setup_class(self):
        self.faker = faker.Faker('zh_CN')

        self.name = self.faker.name()

    def setup(self):
        self.app = App().start()
        self.main_page = self.app.goto_main()

    def teardown_class(self):
        self.app.stop()

    def test_addcontact(self):
        print(self.name)
        phone_number = self.faker.phone_number()
        tips = self.main_page.\
            go_to_contact_page().\
            go_to_add_member_page().\
            go_to_edit_member_page().\
            edit_member(self.name,phone_number).\
            get_toast_tips()
        assert "添加成功" == tips

    def test_delcontact(self):
        name = self.name
        result = self.main_page. \
            go_to_contact_page().\
            search_contact(name).\
            go_to_edit_member_page().\
            delete_member().\
            get_no_result(name)
        assert "无搜索结果" == result
