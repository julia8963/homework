'''
@file:wework.py
@author:'Julia'
@time:6/11/2023
@desc:
'''
import logging

import requests


class Wework:

    def __init__(self):
        self.token = self.get_access_token()


    def get_access_token(self):
        logging.info('初始化,获取access_token')
        self.base_url = "https://qyapi.weixin.qq.com/cgi-bin/"
        url =f"{self.base_url}/gettoken?corpid=wwdd1bae0fed8f19a3&corpsecret=p3NIazUP4ibtwjVRK4V50goGbDxf7T1YJkGdOnXjbRY"
        response = requests.request("GET", url)
        self.access_token = response.json()['access_token']
        logging.info(f'获取到的access_token为{self.access_token}')
        return self.access_token