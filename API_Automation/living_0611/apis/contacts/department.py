'''
@file:department.py
@author:'Julia'
@time:6/11/2023
@desc:
'''
from API_Automation.living_0611.apis.wework import Wework


class Department(Wework):

    def create(self):
        ...

    def update(self):
        ...

    def delete(self):
        ...

    def get(self):
        ...


