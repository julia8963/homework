'''
@file:element_locators.py
@author:'Julia'
@time:5/15/2023
@desc: 八大定位方法及3种等待
'''

import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome()
driver.implicitly_wait(30)  # 隐式等待
driver.get('https://www.ceshiren.com')
driver.find_element(By.ID,'ember26').click()  # 通过ID定位
time.sleep(2) #强制等待
driver.get('https://www.baidu.com')
driver.find_element(By.NAME,'tj_briicon').click()  # 通过name定位
print(driver.current_window_handle)
print(driver.window_handles, driver.title)
driver.switch_to.window(driver.window_handles[-1])
driver.find_element(By.LINK_TEXT,'百度智能门户').click()  # 通过LINK_TEXT定位
text_new = driver.find_element(By.CSS_SELECTOR, '.new').text  # 通过CSS_SELECTOR定位
print(text_new)
driver.find_element(By.XPATH, '//*[contains(text(),"百度健康")]').click()  # 通过XPATH定位
time.sleep(4)
driver.switch_to.window(driver.window_handles[0])
WebDriverWait(driver, 10).until(
    expected_conditions.presence_of_element_located((By.ID, 'kw'))
)

