'''
@file:browser_control.py
@author:'Julia'
@time:5/15/2023
@desc:
'''
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com')  # 打开浏览器
time.sleep(2)
driver.refresh()  # 刷新
time.sleep(2)
driver.find_element(By.ID, 'kw').send_keys('ui')
driver.find_element(By.ID, 'su').click()
time.sleep(2)
driver.back()  # 回退
time.sleep(2)
driver.maximize_window()  # 最大化
time.sleep(2)
driver.minimize_window()  # 最小化
time.sleep(2)
driver.quit()
