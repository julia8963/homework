"""
@file:test_department.py
@author:'Julia'
@time:6/11/2023
@desc:
1. 通过企业微信接口文档获取登录权限 access_token，并进行响应断言。
2. 创建部门，传入上一个接口的结果数据access_token，并使用jsonpath表达式获取响应结果的errmsg进行断言。
3. 生成对应的allure报告，显示log日志打印输出获取的access_token值。
4. 获取子部门ID列表，响应结果使用jsonschame进行断言验证。
5. 生成对应的allure报告，显示log日志打印输出获取的access_token值。
pytest .\test_department.py --clean-alluredir --alluredir ./reports/report
allure serve "报告路径"

"""
import json
import logging

import jsonpath
import requests

from homework_api.utils_jsonschema import UtilsJsonschema


class TestDepartment:

    def setup_class(self):
        logging.info('初始化企业ID')
        self.corpid = 'wwdd1bae0fed8f19a3'
        logging.info('初始化secret')
        self.secret = 'p3NIazUP4ibtwjVRK4V50goGbDxf7T1YJkGdOnXjbRY'
        self.base_url = "https://qyapi.weixin.qq.com"
        logging.info('开始获取token')
        uri = "/cgi-bin/gettoken"
        url = self.base_url + uri
        params = {
            "corpid": self.corpid,
            "corpsecret": self.secret
        }
        logging.info('发送获取接口请求')
        response = requests.request("GET", url, params=params)
        self.access_token = response.json()["access_token"]
        logging.info(f'返回的access_token为：{self.access_token}')

    def test_create_department(self):
        logging.info('开始创建企业：')
        url = f'{self.base_url}/cgi-bin/department/create?access_token={self.access_token}'
        logging.info(f'请求的url为: {url}')
        body = {
            "name": "广州研发中心",
            "name_en": "RDGZ",
            "parentid": 1,
            "order": 1,
            "id": 201
        }
        logging.info(f'请求的body为： {body}')
        response = requests.post(url=url, json=body)
        logging.info(f'请求返回的结果为：{response.text}')
        logging.info('使用jsonpath表达式获取响应结果的errmsg进行断言，验证部门创建成功：')
        assert 'created' in jsonpath.jsonpath(response.json(), '$..errmsg')

    def test_validate_child_departments_schema(self):
        logging.info('获取子部门ID列表')
        url = f"{self.base_url}/cgi-bin/department/simplelist?access_token={self.access_token}"
        response = requests.get(url)
        logging.info('响应结果使用jsonschame进行断言验证')
        schema = json.load(open('department_list_schema.json','r'))
        result = UtilsJsonschema.validate_schema(schema=schema, obj=response.json())
        assert result

