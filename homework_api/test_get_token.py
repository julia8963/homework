'''
@file:test_get_token.py
@author:'Julia'
@time:6/11/2023
@desc:
'''
import logging

import allure
import requests


class TestGetToken:

    def setup(self):
        logging.info('初始化企业ID')
        self.corpid = 'wwdd1bae0fed8f19a3'
        logging.info('初始化secret')
        self.secret = 'p3NIazUP4ibtwjVRK4V50goGbDxf7T1YJkGdOnXjbRY'
        self.base_url = "https://qyapi.weixin.qq.com"

    def teardown(self):
        ...

    @allure.story('获取token')
    def test_get_token(self):
        logging.info('开始获取token')
        uri = "/cgi-bin/gettoken"
        url = self.base_url + uri
        params = {
            "corpid":self.corpid,
            "corpsecret":self.secret
        }
        logging.info('发送获取接口请求')
        response = requests.request("GET", url, params=params)
        print(response.text)
        logging.info('验证返回response状态码为200')
        assert 200 == response.status_code
        logging.info('验证返回errmsg为ok')
        assert 'ok' == response.json()['errmsg']
        logging.info(f'返回的access_token为：{response.json()["access_token"]}')
