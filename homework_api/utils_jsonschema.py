"""
@file:utils_jsonschema.py
@author:'Julia'
@time:6/11/2023
@desc:
"""
import json
import logging

import genson
from jsonschema.validators import validate


class UtilsJsonschema:

    @classmethod
    def validate_schema(cls, schema, obj):
        try:
            logging.info(f'json schema 验证')
            validate(schema=schema, instance=obj)
            return True
        except Exception as e:
            logging.info(f'json schema 验证出错{e.args}')
            print(e)
            return False

    @classmethod
    def generate_schema(cls, obj, path):
        builder = genson.SchemaBuilder()
        builder.add_object(obj)
        schema = builder.to_schema()
        json.dump(schema, open(path, 'w'))
        # with open('department_list_schema.json','w',encoding='utf-8') as f:
        #     json.dump(schema,f)


if __name__ == '__main__':
    department_list = {
        "errcode": 0,
        "errmsg": "ok",
        "department_id": [
            {
                "id": 1,
                "parentid": 0,
                "order": 100000000
            },
            {
                "id": 2,
                "parentid": 1,
                "order": 100000000
            }
        ]
    }
    # UtilsJsonschema.generate_schema(department_list, 'department_list_schema.json')
    # print(json.load(open('department_list_schema.json', 'r')))
    result = UtilsJsonschema.validate_schema(
        schema=json.load(open('department_list_schema.json', 'r')),
                         obj=department_list)
    print(result)
